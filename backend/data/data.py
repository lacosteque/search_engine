import re
import os
import csv
import glob
import asyncio
import datetime


async def search_file(extension: str) -> str:

    os.chdir(".")

    for file in glob.glob(f"./data/*.{extension}", recursive=True):
        if '~$' not in file:
            return file

def read_csv(path: str) -> tuple:
    with open(path, newline='', encoding='utf-8') as csvfile:
        rows = csv.reader(csvfile)
        next(rows)

        results = tuple(
                {
                 'texts': row[0], 
                 'created_date': datetime.datetime.strptime(row[1], '%Y-%m-%d %H:%M:%S'), 
                 'rubrics': re.sub("'|\[|\]| ", '', row[2]).split(',') 
                } for row in rows)


        return results

        
async def get_posts():
    loop = asyncio.get_event_loop()
    path = await search_file('csv')
    if path :
        data = await loop.run_in_executor(None, read_csv, path)
        return data
    else: 
        print('file not found')



async def found(n):
  found = [' результат', ' результата', ' результатов']
  if n % 10 == 1 and n % 100 != 11:
      p = 0
  elif 2 <= n % 10 <= 4 and (n % 100 < 10 or n % 100 >= 20):
      p = 1
  else:
      p = 2
  return found[p]



