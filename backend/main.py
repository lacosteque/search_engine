from __init__ import app
from services import connection_check
from services import disconnected_services
from services import create_test_data
from services import remove_test_data
from core.config import settings
from elasticsearch import AsyncElasticsearch
import uvicorn


@app.on_event("startup")
async def app_startup():
    await connection_check()
    await create_test_data()
    

@app.on_event("shutdown")
async def app_shutdown():
    await remove_test_data()
    await disconnected_services()

if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=8000, log_level="info")
