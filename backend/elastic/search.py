from core.config import settings
from elasticsearch import AsyncElasticsearch
from elasticsearch.helpers import async_bulk, async_scan


class ES:


    def __init__(self):
        self.es = AsyncElasticsearch(settings.ELASTICSEARCH_URL, 
            basic_auth=(settings.ELASTICSEARCH_LOGIN, settings.ELASTICSEARCH_PASSWORD)) \
                    if settings.ELASTICSEARCH_URL else None
        self.index = settings.ELASTICSEARCH_INDEX

    
    def client(self):
        yield self.es


    async def check_start_es(self):
        if not self.es:
            return False
        else:
            return True


    async def disconnect(self):
        if await self.check_start_es():
            await self.es.close()


    async def check_connected(self):
        if await self.check_start_es():
            if await self.es.ping():
                print('Yay Connect')
            else:
                print('Awww it could not connect!')


    async def create_index(self) -> bool:
        if await self.check_start_es():
            created = False
    
            settings = {
    "settings": {
        "number_of_shards": 1,
        "number_of_replicas": 0,


        "analysis": {
            "filter": {
                "russian_stop": {
                    "type": "stop",
                    "stopwords": "_russian_"
                },

                "russian_stemmer": {
                    "type": "stemmer",
                    "language": "russian"
                }
            },
            "analyzer": {
                "rebuilt_russian": {
                    "tokenizer": "standard",
                    "filter": ["lowercase", "russian_stop", "russian_stemmer"]
                }
            }
        }
    },

    "mappings": {
       
            "_source": { "enabled": False },
            "properties": {
                "text": {
                    "type": "text",
                    "analyzer": "rebuilt_russian"
                }
            }
        
    }
}

            try:
                if not await self.es.indices.exists(index=self.index):
                    await self.es.indices.create(index=self.index,  body=settings) #ignore=400,
                    print('Created Index')
                created = True
            except Exception as ex:
                print(str(ex))
            finally:
                return created


    async def delete_index(self) -> None:
        if await self.check_start_es():
            await self.es.indices.delete(index=self.index)


    async def add_documents_to_index(self, documents: list) -> None : 
        if await self.check_start_es():
            payload = [{field.replace('id', '_id'): getattr(document, field) for field in document.__searchable__} for document in documents]
            await async_bulk(self.es, payload, index=self.index)
            

    async def remove_document_from_index(self, id: int):
        if await self.check_start_es():
            await self.es.delete(index=self.index, id=id)


    async def query_index(self, query: str) -> list:
        result = async_scan(self.es, query={"query": {"multi_match": {"query": query}}}, index=self.index)
        ids = [int(id['_id']) async for id in result]
        return ids

es = ES()
