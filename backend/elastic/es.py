from elasticsearch import AsyncElasticsearch

async def connect_elasticsearch():
    _es = None
    _es = AsyncElasticsearch(
            'http://localhost:9200',
            basic_auth=('elastic', '1488')
            )
    if await _es.ping():
        print('Yay Connect')
    else:
        print('Awww it could not connect!')
    return _es


async def create_index(es, index_name = 'example'):
    created = False
    settings = {
    "settings": {
        "number_of_shards": 1,
        "number_of_replicas": 0,

        "analysis": {
            "filter": {
                "russian_stop": {
                    "type": "stop",
                    "stopwords": "_russian_"
                },

                "russian_stemmer": {
                    "type": "stemmer",
                    "language": "russian"
                }
            },
            "analyzer": {
                "rebuilt_russian": {
                    "tokenizer": "standard",
                    "filter": ["lowercase", "russian_stop", "russian_stemmer"]
                }
            }
        }
    },

    "mappings": {
        "texts": {
            "_source": { "enabled": False },
            "properties": {
                "text": {
                    "type": "string",
                    "index": "rebuilt_russian"
                }
            }
        }
    }
}
    try:
        if not await es.indices.exists(index=index_name):
            await es.indices.create(index=index_name, ignore=400, body=settings)
            print('Created Index')
        created = True
    except Exception as ex:
        print(str(ex))
    finally:
        await es.close()
        return created

async def store_record(es, iD, index_name, record):
    try:
        outcome = await es.index(index=index_name, id=iD, document=record)
        await es.close()
    except Exception as ex:
        print('Error in indexing data')
        print(str(ex))


async def query_index(index, query, page, per_page):
    es = await connect_elasticsearch()

    search = await es.search(
        index=index,
        body={'query': {'multi_match': {'query': query, 'fields': ['*']}},
             'from': (page - 1) * per_page, 'size': per_page})
    ids = [int(hit['_id']) for hit in search['hits']['hits']]

    await es.close()
    return ids, search['hits']['total']['value']


async def es_search(query) -> tuple:

    es = await connect_elasticsearch()

    response = await es.search(
        #index="my_index",
        index = "za_mir",
        query = {"match": {'texts': query}}
    )

    page = 1
    per_page = 100

    search = await es.search(
        index="za_mir",
        body={'query': {'multi_match': {'query': query, 'fields': ['*']}},
             'from': (page - 1) * per_page, 'size': per_page})
    
    iDs = [int(hit['_id']) for hit in search['hits']['hits']]
    
    
    
    #iDs = [int(i['_id']) for i in response['hits']['hits']]

    #await es.indices.delete('za_mir')
    
    await es.close()
    
    return iDs

    
