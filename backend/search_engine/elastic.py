from core.config import settings
from elasticsearch import AsyncElasticsearch
from elasticsearch.helpers import async_bulk, async_scan


async def elastic_client():
    client = AsyncElasticsearch(settings.ELASTICSEARCH_URL,basic_auth=(settings.ELASTICSEARCH_LOGIN, settings.ELASTICSEARCH_PASSWORD), timeout=30)
    yield client
    await client.close()\
        if settings.ELASTICSEARCH_URL else None
    


async def check_init_elastic(client: AsyncElasticsearch) -> bool:
    if not client:
        return False
    else:
        return True


async def delete_index(client: AsyncElasticsearch) -> None:
    if not await check_init_elastic(client):
        return 
    await client.indices.delete(index='*')



async def add_documents_to_index(client: AsyncElasticsearch, documents: list) -> None : 
    if not await check_init_elastic(client):
        return 
    payload = [{field.replace('id', '_id'): getattr(document, field) for field in document.__searchable__} for document in documents]
    await async_bulk(client, payload, index=settings.ELASTICSEARCH_INDEX)
 


async def remove_document_from_index(client: AsyncElasticsearch, id: int) -> None :
    if await check_init_elastic(client):
        await client.delete(index=settings.ELASTICSEARCH_INDEX, id=id)
        


async def query_index(client: AsyncElasticsearch, query: str) -> list:
    if not await check_init_elastic(client):
        return 
    body = {"query": {"multi_match": {"query": query}}}
    result = async_scan(client, query=body, index=settings.ELASTICSEARCH_INDEX)
    ids = [int(id['_id']) async for id in result]
    await client.close()

    return ids



class ES:


    async def disconnect(self):
        if await self.check_start_es():
            await self.es.close()


    async def check_connected(self):
        if await self.check_start_es():
            if await self.es.ping():
                print('Yay Connect')
            else:
                print('Awww it could not connect!')


    

es = ES()
