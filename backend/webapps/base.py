from fastapi import APIRouter
from webapps.homepage import route_homepage
from webapps.documents import route_documents


api_router = APIRouter()
api_router.include_router(route_homepage.router, prefix="", tags=["home-webapp"])
api_router.include_router(route_documents.router, prefix="", tags=["documents-webapp"])
