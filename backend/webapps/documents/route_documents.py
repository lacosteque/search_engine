from fastapi import Request, Response
from fastapi import APIRouter
from fastapi import Depends
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from sqlalchemy.ext.asyncio import AsyncSession
from db.session import get_session
from elasticsearch import AsyncElasticsearch
from core.config import settings
from elastic.search import es
from services import search_documents
from services import delete_documents
from search_engine.elastic import elastic_client


templates = Jinja2Templates(directory="templates")
router = APIRouter(include_in_schema=False)


@router.delete("/delete/{id}")
async def delete(
    request: Request, 
    db: AsyncSession = Depends(get_session),
    engine_client: AsyncElasticsearch = Depends(es.client)
    ):
    data = await request.json()
    id = data.get('id')
    result = await delete_documents(id, engine_client, db)
    return templates.TemplateResponse(
            'components/alert.html', {'request': request, 'id': id})
    
    
@router.post("/search/")

async def search(
    request: Request,
    db: AsyncSession = Depends(get_session), 
    engine_client: AsyncElasticsearch = Depends(elastic_client)
    ):

    data = await request.json()
    query = data.get('query')
    
    results = await search_documents(query, engine_client, db)

    await engine_client.close()

    return templates.TemplateResponse(
        'components/result.html', {'request': request, 'results': results})















