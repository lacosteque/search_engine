#!/bin/bash
set -e
url="${ELASTICSEARCH_URL}"
cmd="$@"
>&2 echo "Check Elasticsearch for available"
until curl -s -I "$url" | grep -q 'HTTP/1.1 401 Unauthorized'; do
  >&2 echo "Elasticsearch is unavailable - sleeping"
  sleep 10
done
>&2 echo "Elastic - UP"
exec $cmd
