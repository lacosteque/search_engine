import os
from pathlib import Path
from dotenv import load_dotenv


env_path = Path(".") / ".env"
load_dotenv(dotenv_path=env_path)


class Settings:

    PROJECT_NAME: str = "Search Engine by Ilia Nikitin"
    PROJECT_VERSION: str = "0.0.2"
    POSTGRES_USER: str = os.getenv("POSTGRES_USER")
    POSTGRES_PASSWORD = os.getenv("POSTGRES_PASSWORD")
    POSTGRES_SERVER: str = os.getenv("POSTGRES_SERVER", "localhost")
    POSTGRES_PORT: str = os.getenv(
        "POSTGRES_PORT", 5432
    )  
    POSTGRES_DB: str = os.getenv("POSTGRES_DB")
    DATABASE_URL = f"postgresql+asyncpg://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{POSTGRES_SERVER}:{POSTGRES_PORT}/{POSTGRES_DB}"

    ELASTICSEARCH_URL = str = os.getenv("ELASTICSEARCH_URL")
    ELASTICSEARCH_LOGIN = str = os.getenv("ELASTICSEARCH_LOGIN")
    ELASTICSEARCH_PASSWORD = str = os.getenv("ELASTICSEARCH_PASSWORD")
    ELASTICSEARCH_INDEX = str = os.getenv("ELASTICSEARCH_INDEX")

settings = Settings()
