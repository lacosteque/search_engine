from api.search import route_search
from fastapi import APIRouter


api_router = APIRouter()
api_router.include_router(route_search.router, prefix="/search", tags=["search"])
