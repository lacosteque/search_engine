$(function () {
    $("#form").submit(function (e) {
        e.preventDefault();
        var form = $(this);
        var value = $("#query").val();
        var data = {
            query: value,
        };
      
        $.ajax({
            url: form.attr("action"),
            type: form.attr("method"),
            data: JSON.stringify(data),
            dataType: "html",
            beforeSend: function (xhr, settings) {
                $(".loader").css("visibility", "inherit");
            },
        })
            .done(function (result) {
                $(".loader").css("visibility", "hidden");
                $("#result").html(result);
            })
            .fail(function (xhr, ajaxOptions, thrownError) {
                console.log(argument);
            });
    });
});

$(document).ajaxComplete(function () {
    $(".remove").on("click", function () {
        var id = $(this).data("id");
        var data = {
            id: id,
        };
        $.ajax({
            url: "/delete/" + id,
            type: "DELETE",
            data: JSON.stringify(data),
            dataType: "html",
            global: false,
            beforeSend: function (xhr, settings) {},
        })
            .done(function (result) {
                $(".footer").html(result);
                $(".toast").toast("show");
                $("div#" + id).fadeTo(500, 0.01, function () {
                    $(this).remove();
                });
            })
            .fail(function (xhr, ajaxOptions, thrownError) {
                console.log(argument);
            });
    });
});
