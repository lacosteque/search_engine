from db.utils import check_db_connected
from db.utils import check_db_disconnected
from db.utils import write_documents
from db.utils import create_tables
from db.utils import drop_tables

from db.repository.documents import get_all_documents
from db.repository.documents import search_documents_by_id
from db.repository.documents import delete_documents_by_id

from search_engine.elastic import query_index
from search_engine.elastic import elastic_client
from search_engine.elastic import remove_document_from_index
from search_engine.elastic import add_documents_to_index
from search_engine.elastic import delete_index

from elasticsearch import AsyncElasticsearch
from sqlalchemy.ext.asyncio import AsyncSession


async def search_documents(query: str, engine_client: AsyncElasticsearch, db: AsyncSession) -> list|str:

    ids = await query_index(engine_client, query)

    if ids:
        result = await search_documents_by_id(ids, db)
    else:
        result = f'<div class="text-center mt-5"><p class="lead">По запросу <mark>{query}</mark> ничего не найдено!</p></div>'

    return result


async def delete_documents(id: int, engine_client: AsyncElasticsearch, db: AsyncSession) -> None:

    await remove_document_from_index(engine_client, id)
    await delete_documents_by_id(id, db)



async def connection_check():
    await check_db_connected()


async def disconnected_services():
    await check_db_disconnected()


async def create_test_data() -> None:
    await create_tables()
    await write_documents('document')

    documents = await get_all_documents()
    async for client in elastic_client():
        await add_documents_to_index(client, documents)


async def remove_test_data() -> None:
    await drop_tables()
    async for client in elastic_client():
        await delete_index(client)
