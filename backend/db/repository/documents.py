from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select
from sqlalchemy import delete
from db.session import async_session
from db.models.documents import Document


async def get_all_documents() -> list:
    async with async_session() as db:
        stmt = select(Document)
        rows = await db.execute(stmt)
        documents = rows.scalars().all()
        return documents


async def delete_documents_by_id(id: int, db: AsyncSession) -> bool: 
    stmt = select(Document).filter(Document.id == id)
    existing_document = await db.execute(stmt)
    if not existing_document.first():
        return False
    stmt = delete(Document).filter(Document.id == id)
    result = await db.execute(stmt)
    await db.commit()
    return True


async def search_documents_by_id(ids: list,  db: AsyncSession, limit: int = 20 ) -> list:
    stmt = select(Document).filter(Document.id.in_(ids)).order_by(Document.created_date.desc()).limit(limit)
    rows = await db.execute(stmt)
    documents = rows.scalars().all()
    return documents