from db.base_class import Base
from sqlalchemy import Column
from sqlalchemy import DateTime
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import ARRAY


class Document(Base):
    __searchable__ = ['texts', 'id']
    id = Column(Integer, primary_key=True, index=True)
    rubrics = Column(ARRAY(String), nullable=False)
    texts = Column(String, nullable=False)
    created_date = Column(DateTime, nullable=False)