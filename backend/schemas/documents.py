from typing import Optional
from pydantic import BaseModel
from datetime import date


class DocumentBase(BaseModel):
    id: Optional[int] = None
    rubrics: Optional[list] = None
    texts: Optional[str] = None
    created_date: Optional[date] = None



class DocumentCreate(DocumentBase):
    id: int
    rubrics: list
    texts: str
    created_date: date
