FROM python:3.10.6-slim-bullseye

LABEL Author="Ilia Nikitin"

ENV PYTHONBUFFERED 1

WORKDIR /app
COPY ./backend /app

RUN python -m pip install --upgrade pip setuptools wheel
RUN pip --no-cache-dir install -r requirements.txt
RUN apt update && \
        apt upgrade -y && \
        apt install curl -y && \
        apt clean && \
        rm -rf /var/lib/apt/lists/* && \
        chmod +x entrypoint.sh

EXPOSE 8000

ENTRYPOINT [ "./entrypoint.sh" ]

CMD ["python", "main.py"]
